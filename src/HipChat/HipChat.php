<?php
namespace Nakima\HipChatBundle\HipChat;

/**
 * @author xgonzalez@nakima.es
 */

class HipChat {

    private $token;

    public function __construct($token) {
        $this->token = $token;
    }

    public function sendNotification($message, $room, $color) {

        // valid colors 'yellow', 'green', 'red', 'purple', 'gray', 'random'
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, "https://api.hipchat.com/v2/room/$room/notification?auth_token=$this->token");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Connection: Keep-Alive'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            "color" => $color,
            "message_format" => "text",
            "message" => $message
        ]));

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}

