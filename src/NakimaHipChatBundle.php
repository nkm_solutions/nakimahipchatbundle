<?php
namespace Nakima\HipChatBundle;

/**
 * @author xgonzalez@nakima.es
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaHipChatBundle extends Bundle {}
